;;; popup-imenu-autoloads.el --- automatically extracted autoloads
;;
;;; Code:
(add-to-list 'load-path (directory-file-name (or (file-name-directory #$) (car load-path))))

;;;### (autoloads nil "popup-imenu" "popup-imenu.el" (23062 56764
;;;;;;  244182 153000))
;;; Generated autoloads from popup-imenu.el

(autoload 'popup-imenu "popup-imenu" "\
Open the popup window with imenu items.

\(fn)" t nil)

;;;***

;; Local Variables:
;; version-control: never
;; no-byte-compile: t
;; no-update-autoloads: t
;; End:
;;; popup-imenu-autoloads.el ends here
