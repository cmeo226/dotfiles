
;; -- mode: emacs-lisp -*-
;; Simple .emacs configuration

;; ---------------------
;; -- Global Settings --
;; ---------------------

;;; Code:
(setq package-enable-at-startup nil)
(when (>= emacs-major-version 24)
  (setq package-archives '(;;("melpa" . "http://stable.melpa.org/packages/")
			   ("melpa" . "http://melpa.milkbox.net/packages/")
			   ("marmalade" . "http://marmalade-repo.org/packages/")
			   ("gnu" . "http://elpa.gnu.org/packages/")))
  (package-initialize))

(add-to-list 'load-path "~/.emacs.d/lisp")
(add-to-list 'load-path "~/.emacs.d/elpa")

(require 'cl)
(require 'ido)
(require 'ffap)
(require 'uniquify)
(require 'ansi-color)
(require 'recentf)
(require 'linum)
(require 'smooth-scrolling)
(require 'dired-x)
(require 'compile)
(require 'whitespace)
(setq whitespace-style '(face empty tabs lines-tail trailing))
; (global-whitespace-mode t)

(ido-mode t)
(menu-bar-mode -1)
(normal-erase-is-backspace-mode 0)
(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(setq column-number-mode t)
(setq inhibit-startup-message t)
(setq save-abbrevs nil)
(setq show-trailing-whitespace t)
(setq suggest-key-bindings t)
(setq vc-follow-symlinks t)
(setq-default indent-tabs-mode nil)
(setq backup-directory-alist
      `((".*" . ,temporary-file-directory)))
(setq auto-save-file-name-transforms
      `((".*" ,temporary-file-directory t)))

(load-theme 'wombat t)
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:inherit autoface-default :strike-through nil :underline nil :slant normal :weight normal :height 120 :width normal :family "helvetica" :background "#101010"))))
 '(column-marker-1 ((t (:background "red"))))
 '(flymake-errline ((((class color) (background light)) (:background "red"))))
 '(font-lock-comment-face ((t (:slant italic))))
 '(font-lock-warning-face ((t (:weight bold :foreground "red"))))
 '(fundamental-mode-default ((t (:inherit default))))
 '(highlight ((((class color) (min-colors 8)) (:background "white" :foreground nil))))
 '(isearch ((((class color) (min-colors 8)) (:background "yellow" :foreground "black"))))
 '(linum ((t (:foreground "black" :weight bold))))
 '(linum-highlight-face ((t (:background "black" :foreground "white" :weight bold))))
 '(region ((((class color) (min-colors 8)) (:background "black" :foreground nil))))
 '(secondary-selection ((((class color) (min-colors 8)) (:background "gray" :foreground "cyan"))))
 '(show-paren-match ((((class color) (background light)) (:background "black"))))
 '(vertical-border ((t nil))))

;; ------------
;; -- Macros --
;; ------------

(load "defuns-config.el")

(fset 'align-equals "\C-[xalign-regex\C-m=\C-m")
(global-set-key "\M-=" 'align-equals)
(global-set-key "\C-x\C-m" 'execute-extended-command)
(global-set-key "\C-c;" 'comment-or-uncomment-region)
(global-set-key "\M-n" 'next5)
(global-set-key "\M-p" 'prev5)
(global-set-key "\M-o" 'other-window)
(global-set-key "\M-i" 'back-window)
(global-set-key "\C-z" 'zap-to-char)
(global-set-key "\C-h" 'backward-delete-char)
(global-set-key "\M-d" 'delete-word)
(global-set-key "\M-h" 'backward-delete-word)
(global-set-key "\M-u" 'zap-to-char)


;; ----------------------
;; -- Add line numbers --
;; ----------------------
(global-linum-mode t)
(require 'hlinum)
(hlinum-activate)

;; ---------------------------
;; -- JS Mode configuration --
;; ---------------------------
(defun enable-minor-mode (my-pair)
  "Enable minor mode if filename match the regexp.  MY-PAIR is a cons cell (regexp . minor-mode)."
  (if (buffer-file-name)
      (if (string-match (car my-pair) buffer-file-name)
          (funcall (cdr my-pair)))))

(eval-after-load 'web-mode
  '(add-hook 'web-mode-hook
             (lambda ()
               ;; (add-hook 'before-save-hook 'web-beautify-html-buffer t t)
               (enable-minor-mode '("\\.jsx?\\'" . prettier-js-mode))
               (enable-minor-mode '("\\.vue$" . prettier-js-mode)))))

(add-to-list 'load-path "~/.emacs.d/jade-mode") ;; github.com/brianc/jade-mode
(require 'sws-mode)
(require 'jade-mode)
(add-to-list 'auto-mode-alist '("\\.js$"   . web-mode))
(add-to-list 'auto-mode-alist '("\\.vue$"  . web-mode))
(add-to-list 'auto-mode-alist '("\\.styl$" . sws-mode))
(add-to-list 'auto-mode-alist '("\\.jade$" . jade-mode))

;; -----------------------
;; -- Git configuration --
;; -----------------------
(require 'git-commit)
(add-hook 'git-commit-mode-hook 'flyspell-mode)
;; (add-hook 'git-commit-mode-hook (lambda () (toggle-save-place 0)))

;; -------------------------
;; -- Julia configuration --
;; -------------------------
(require 'julia-mode)

;; ------------------
;; -- Code Folding --
;; ------------------
(require 'yafolding)

;; ------------------------
;; -- Golang integration --
;; ------------------------
(with-eval-after-load 'go-mode
  (require 'go-autocomplete)
  (require 'go-oracle)
  (setq go-oracle-scope "."))

(add-hook 'go-mode-hook
	  (lambda ()
	    (require 'auto-complete-config)
	    (ac-config-default)
	    (setq gofmt-command "goimports")
	    (add-hook 'before-save-hook 'gofmt-before-save)
	    (auto-complete-mode 1)
	    (if (not (string-match "go" compile-command))
		(set (make-local-variable 'compile-command)
		     "go generate && go build -v && go test -v && go vet"))
	    (local-set-key (kbd "M-.") 'godef-jump)))

;; ----------------------------
;; -- Enable global flycheck --
;; ----------------------------
(add-hook 'prog-mode-hook 'flycheck-mode)
(add-hook 'text-mode-hook 'flycheck-mode)

;; --------------------------------
;; -- adding protocolbuffer mode --
;; --------------------------------
(require 'protobuf-mode)
(add-to-list 'auto-mode-alist '("\\.proto$" . protobuf-mode))

;; -----------------
;; -- removing vc --
;; -----------------
(setq vc-handled-backends ())

;; ---------------------
;; -- Typescript mode --
;; ---------------------
(add-hook 'typescript-mode-hook
	  (lambda ()
	    (tide-setup)
	    (flycheck-mode +1)
	    (setq flycheck-check-syntax-automatically '(save mode-enabled))
	    (eldoc-mode +1)
	    ;; aligns annotation to the right hand side
	    (setq company-tooltip-align-annotations t)
	    ;; formats the buffer before saving
	    (add-hook 'before-save-hook 'tide-format-before-save)))
	    ;; format options
	    (setq tide-format-options
		  '(:insertSpaceAfterFunctionKeywordForAnonymousFunctions
		    t
		    :placeOpenBraceOnNewLineForFunctions
		    nil))
	    ;; see https://github.com/Microsoft/TypeScript/blob/cc58e2d7eb144f0b2ff89e6a6685fb4deaa24fde/src/server/protocol.d.ts#L421-473 for the full list available options


	    ;; format options
	    (setq tide-format-options
		  '(:insertSpaceAfterFunctionKeywordForAnonymousFunctions
		    t
		    :placeOpenBraceOnNewLineForFunctions
		    nil))
	    ;; see https://github.com/Microsoft/TypeScript/blob/cc58e2d7eb144f0b2ff89e6a6685fb4deaa24fde/src/server/protocol.d.ts#L421-473 for the full list available options
	    ;; company is an optional dependency. You have to
	    ;; install it separately via package-install
	    ;; (company-mode-on)))

;; Add python-mode for BUILD and WORKSPACE bazel files
(add-to-list 'auto-mode-alist '("BUILD" . python-mode))
(add-to-list 'auto-mode-alist '("WORKSPACE"  . python-mode))
(add-to-list 'auto-mode-alist '("\\.bzl$"  . python-mode))

;; Tide can be used along with web-mode to edit tsx files
;; (require 'web-mode)
(add-to-list 'auto-mode-alist '("\\.tsx\\'" . web-mode))
(add-to-list 'auto-mode-alist '("\\.php\\'" . web-mode))
(add-hook 'web-mode-hook
	  (lambda ()
	    (when (string-equal "tsx" (file-name-extension buffer-file-name))
	      (tide-setup)
	      (flycheck-mode +1)
	      (setq flycheck-check-syntax-automatically '(save mode-enabled))
	      (eldoc-mode +1))))

;; -------------------
;; -- HTML web mode --
;; -------------------
(add-to-list 'auto-mode-alist '("\\.html$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.htm$" . web-mode))

(provide 'init)
;;; init.el ends here
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (web-beautify prettier-js vue-mode systemd dockerfile-mode company elm-mode groovy-mode feature-mode cargo racer flycheck-rust flycheck tide toml-mode yaml-mode yafolding web-mode typescript popup-imenu popup-complete php-mode magit jsx-mode js3-mode js-comint go-complete go-autocomplete flymake-phpcs flymake-php fish-mode company-web company-go color-theme-github bubbleberry-theme))))

;; --------------
;; -- CSS mode --
;; --------------
(setq css-indent-offset 2)

;; ---------------------------------
;; -- rust-mode.el initialization --
;; ---------------------------------
(add-to-list 'load-path "~/.emacs.d/lisp/rust-mode/")
(autoload 'rust-mode "rust-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.rs\\'" . rust-mode))
(add-hook 'rust-mode
	  (lambda ()
	    (require 'rust-mode)
	    (define-key rust-mode-map (kbd "TAB") #'company-indent-or-complete-common)
	    (setq company-tooltip-align-annotations t)
	    (setq rust-format-on-save t)
	    (add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
	    (add-hook 'rust-mode-hook 'cargo-minor-mode)
	    (add-hook 'rust-mode-hook #'racer-mode)
	    (add-hook 'racer-mode-hook #'eldoc-mode)
	    (add-hook 'racer-mode-hook #'company-mode)))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (add-to-list 'load-path "~/.emacs.d/lisp/flycheck-rust/")    ;;
;; (autoload 'flycheck-rust "flycheck-rust" nil t)	        ;;
;; (add-to-list 'auto-mode-alist '("\\.rs\\'" . flycheck-rust)) ;;
;; (add-hook 'flycheck-rust 'flycheck-mode)		        ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ------------------
;; -- dart-mode.el --
;; ------------------
(add-to-list 'load-path "~/.emacs.d/lisp/dart-mode/")
(autoload 'dart-mode "dart-mode" nil t)
(add-to-list 'auto-mode-alist '("\\.dart\\'" . dart-mode))
(add-hook 'dart-mode-hook
	  (lambda ()
	    (setq dart-enable-analysis-server t)
	    (flycheck-mode +1)
	    (add-hook 'before-save-hook 'dartfmt-before-save)))

(load-file "~/.emacs.d/lisp/flow-for-emacs/flow.el")

;; --------------
;; -- elm-mode --
;; --------------
(add-hook 'elm-mode-hook
	  (lambda ()
	    (setq elm-format-on-save t)
	    (setq elm-sort-imports-on-save t)
	    (company-mode)
	    (auto-complete-mode t)
	    (add-to-list 'company-backends 'company-elm)))
